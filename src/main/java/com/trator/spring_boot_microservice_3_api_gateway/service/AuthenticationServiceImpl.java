package com.trator.spring_boot_microservice_3_api_gateway.service;

import com.trator.spring_boot_microservice_3_api_gateway.model.User;
import com.trator.spring_boot_microservice_3_api_gateway.security.UserMain;
import com.trator.spring_boot_microservice_3_api_gateway.security.jwt.JwtProvider;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationServiceImpl implements AuthenticationService{

    private final AuthenticationManager authenticationManager;
    private final JwtProvider jwtProvider;

    public AuthenticationServiceImpl(AuthenticationManager authenticationManager, JwtProvider jwtProvider) {
        this.authenticationManager = authenticationManager;
        this.jwtProvider = jwtProvider;
    }

    @Override
    public User signInAndReturnJWT(User signInRequest){
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(signInRequest.getUsername(),signInRequest.getPassword())
        );

        UserMain userMain = (UserMain) authentication.getPrincipal();
        String jwt = jwtProvider.generateToken(userMain);

        User sigInUser = userMain.getUser();
        sigInUser.setToken(jwt);

        return sigInUser;
    }
}
