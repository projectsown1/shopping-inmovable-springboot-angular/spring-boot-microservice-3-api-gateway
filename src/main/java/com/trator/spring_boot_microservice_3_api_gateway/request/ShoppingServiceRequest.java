package com.trator.spring_boot_microservice_3_api_gateway.request;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient(
        value="compras-service",
        path="/api/shopping",
        //url="${shopping.service.url}",
        configuration = FeignConfiguration.class
)
public interface ShoppingServiceRequest {

    @PostMapping
    Object saveShopping(@RequestBody Object requestBody);

    @GetMapping("{userId}")
    List<Object> getAllShoppingOfUser(@PathVariable("userId") Long userId);
}
