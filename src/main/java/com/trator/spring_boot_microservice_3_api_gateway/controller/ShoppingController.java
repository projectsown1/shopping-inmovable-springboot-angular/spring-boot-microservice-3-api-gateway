package com.trator.spring_boot_microservice_3_api_gateway.controller;

import com.trator.spring_boot_microservice_3_api_gateway.request.ShoppingServiceRequest;
import com.trator.spring_boot_microservice_3_api_gateway.security.UserMain;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("gateway/shopping")
public class ShoppingController {

    private final ShoppingServiceRequest shoppingServiceRequest;

    public ShoppingController(ShoppingServiceRequest shoppingServiceRequest) {
        this.shoppingServiceRequest = shoppingServiceRequest;
    }

    @PostMapping
    public ResponseEntity<?> saveShopping(@RequestBody Object shopping){
        return new ResponseEntity<>(shoppingServiceRequest.saveShopping(shopping), HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<?> getAllShoppingOfUser(@AuthenticationPrincipal UserMain userMain){
        return ResponseEntity.ok(shoppingServiceRequest.getAllShoppingOfUser(userMain.getId()));
    }
}
