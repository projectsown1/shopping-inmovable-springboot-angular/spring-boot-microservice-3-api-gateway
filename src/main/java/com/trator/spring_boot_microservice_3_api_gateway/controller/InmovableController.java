package com.trator.spring_boot_microservice_3_api_gateway.controller;

import com.trator.spring_boot_microservice_3_api_gateway.request.InmovableServiceRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("gateway/inmovable")
public class InmovableController {

    private final InmovableServiceRequest inmovableServiceRequest;

    public InmovableController(InmovableServiceRequest inmovableServiceRequest) {
        this.inmovableServiceRequest = inmovableServiceRequest;
    }

    @PostMapping
    public ResponseEntity<?> saveInmovable(@RequestBody Object inmovable){
        return new ResponseEntity<>(inmovableServiceRequest.saveInmovable(inmovable), HttpStatus.CREATED);
    }

    @DeleteMapping("{inmovableId}")
    public ResponseEntity<?> deleteInmovable(@PathVariable("inmovableId") Long inmovableId){
        inmovableServiceRequest.deleteInmovable(inmovableId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping()
    public ResponseEntity<?> getAllInmovables(){
        return ResponseEntity.ok(inmovableServiceRequest.getAllInmovable());
    }
}
