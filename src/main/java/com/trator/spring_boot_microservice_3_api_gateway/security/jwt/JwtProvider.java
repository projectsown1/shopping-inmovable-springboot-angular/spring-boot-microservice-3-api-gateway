package com.trator.spring_boot_microservice_3_api_gateway.security.jwt;

import com.trator.spring_boot_microservice_3_api_gateway.model.User;
import com.trator.spring_boot_microservice_3_api_gateway.security.UserMain;
import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;

public interface JwtProvider {

    String generateToken(UserMain auth);

    String generateToken(User user);

    Authentication getAuthentication(HttpServletRequest request);

    boolean isTokenValid(HttpServletRequest request);
}
