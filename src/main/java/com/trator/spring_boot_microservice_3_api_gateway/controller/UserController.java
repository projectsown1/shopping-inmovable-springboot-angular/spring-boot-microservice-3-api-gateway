package com.trator.spring_boot_microservice_3_api_gateway.controller;

import com.trator.spring_boot_microservice_3_api_gateway.model.Role;
import com.trator.spring_boot_microservice_3_api_gateway.security.UserMain;
import com.trator.spring_boot_microservice_3_api_gateway.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("api/user")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PutMapping("change/{role}")
    public ResponseEntity<?> changeRole(@AuthenticationPrincipal UserMain userMain, @PathVariable Role role){
        userService.changeRole(role, userMain.getUsername());

        return ResponseEntity.ok(true);
    }
}
