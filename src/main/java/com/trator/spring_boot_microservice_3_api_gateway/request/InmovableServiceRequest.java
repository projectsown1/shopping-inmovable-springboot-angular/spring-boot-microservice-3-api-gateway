package com.trator.spring_boot_microservice_3_api_gateway.request;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(
        value="inmueble-service",
        path="/api/inmovable",
        //url="${inmovable.service.url}",
        configuration = FeignConfiguration.class
)
public interface InmovableServiceRequest {

    @PostMapping
    Object saveInmovable(@RequestBody Object requestBody);

    @DeleteMapping("{inmovableId}")
    void deleteInmovable(@PathVariable("inmovableId") Long inmovableId);

    @GetMapping
    List<Object> getAllInmovable();
}
